package com.db.awmd.challenge.exception;


import lombok.Getter;

/*
 Account Not Found Exception When tryinng to transfer amount to anther acount that does not exist.
*/
@Getter
public class AccountNotFoundException extends RuntimeException {

    private final String accountId;

    public AccountNotFoundException(String accountId) {
        super("Account Not Found with Id: " + accountId);
        this.accountId = accountId;
    }
}
