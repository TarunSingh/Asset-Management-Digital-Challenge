package com.db.awmd.challenge.exception;

import lombok.Getter;
/*
* Exception when trying to send money to another account when current account have insufficient balance
*/
@Getter
public class InsufficientBalanceException extends RuntimeException {

    private final String accountId;

    public InsufficientBalanceException(String accountId) {
        super(accountId+ " balance must not be negative!");
        this.accountId = accountId;
    }

}