package com.db.awmd.challenge.service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountNotFoundException;
import com.db.awmd.challenge.model.TransferMoneyRequest;
import com.db.awmd.challenge.model.TransferMoneyResponse;
import com.db.awmd.challenge.repository.AccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Transfer Service that responsible for transferring money between accounts
 */
@Service
public class TransferMoneyService {

    private final TransferMoneyTransactionalService transferDomainService;
    private final AccountsRepository accountRepository;
    @Autowired
    private final NotificationService notificationService;
    private Executor executor;

    public TransferMoneyService(TransferMoneyTransactionalService transferDomainService, AccountsRepository accountRepository, NotificationService notificationService,Executor executor) {
        this.transferDomainService = transferDomainService;
        this.accountRepository = accountRepository;
        this.notificationService = notificationService;
        this.executor=executor;
    }

    public TransferMoneyResponse transferMoney(TransferMoneyRequest transferRequest) {
        //Transferrer Account Details
        CompletableFuture<Account> transferrerAccount = CompletableFuture.supplyAsync(() -> {
            return getAccount(transferRequest.getTransferrerAccountId());
        }, executor);
        //Transferred Account Details
        CompletableFuture<Account> transferredAccount = CompletableFuture.supplyAsync(() -> {
            return getAccount(transferRequest.getTransferredAccountId());
        }, executor);
        //Responsible to Transfer Money
        transferDomainService.transferMoney(transferrerAccount.join(), transferredAccount.join(), transferRequest.getTransferAmount());
        //Send Notification to Both Account holder Asyncronousaly
        sendEmailNotification(transferrerAccount.join(), transferredAccount.join(), transferRequest.getTransferAmount());

        return getResponseMaping();
    }

    private Account getAccount(String accountId) {
        Account account = accountRepository.getAccount(accountId);
        //Account Not Found
        if (account == null) {
            throw new AccountNotFoundException(accountId);
        }
        return account;
    }

    private TransferMoneyResponse getResponseMaping() {
        TransferMoneyResponse response = new TransferMoneyResponse();
        response.setResponseCode(200);
        response.setMessage("Successfully Money Transfer");
        return response;
    }

    private void sendEmailNotification(Account transferrerAccount, Account transferredAccount, BigDecimal amount) {
        //Sending Notification to transferrer Account asyncronousaly
        CompletableFuture.runAsync(() -> {
            notificationService.notifyAboutTransfer(transferrerAccount, "An amount of Rs. " + amount + " has been debited from your account " + transferrerAccount.getAccountId());
        });
        //Sending Notification to transferrered Account asyncronousaly
        CompletableFuture.runAsync(() -> {
            notificationService.notifyAboutTransfer(transferredAccount, "An amount of Rs. " + amount + " has been credited to your account " + transferredAccount.getAccountId());
        });
    }
}