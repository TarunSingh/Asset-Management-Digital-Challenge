package com.db.awmd.challenge.service;


import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.InsufficientBalanceException;
import com.db.awmd.challenge.repository.AccountsRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Transfer money from one account to another
 */
@Service
public class TransferMoneyTransactionalService {

    private final AccountsRepository accountRepository;

    public TransferMoneyTransactionalService(AccountsRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void transferMoney(Account transferrerAccount, Account transferredAccount, final BigDecimal transferAmount) {
        //Account must not be overdrawn (Does not support Negative Balance)
        if (transferrerAccount.getBalance().compareTo(transferAmount) < 0) {
            throw new InsufficientBalanceException(transferrerAccount.getAccountId());
        }
        accountRepository.updateAcount(transferrerAccount, transferredAccount, transferAmount);
    }
}