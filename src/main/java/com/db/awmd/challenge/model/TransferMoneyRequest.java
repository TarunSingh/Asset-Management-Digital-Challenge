package com.db.awmd.challenge.model;


import com.db.awmd.challenge.validator.ValidAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
/**
 * Rest Api Request POJO to handle Money transfer from one Account to another Account
 * @Valid is a custom Validator for an account
 */
@Setter
@Getter
public class TransferMoneyRequest {

    @NotBlank(message = "{account.accountfrom.notempty}")
    @ValidAccount
    @JsonProperty(value = "accountFrom")
    private String transferrerAccountId;

    @NotBlank(message = "{account.accountto.notempty}")
    @JsonProperty(value = "accountTo")
    @ValidAccount
    private String transferredAccountId;

    @JsonProperty(value = "transfer-amount")
    @DecimalMin(value = "1",message = "{transfer.amount.value}")
    private BigDecimal transferAmount;

    public String getTransferrerAccountId() {
        return transferrerAccountId;
    }

    public void setTransferrerAccountId(String transferrerAccountId) {
        this.transferrerAccountId = transferrerAccountId;
    }

    public String getTransferredAccountId() {
        return transferredAccountId;
    }

    public void setTransferredAccountId(String transferredAccountId) {
        this.transferredAccountId = transferredAccountId;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }
}