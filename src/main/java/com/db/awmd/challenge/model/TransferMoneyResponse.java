package com.db.awmd.challenge.model;

/**
 * Rest Api Response POJO Class to handle Money transfer from one Account to another Account
 */
public class TransferMoneyResponse  {
    private Integer responseCode;
    private String message;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
