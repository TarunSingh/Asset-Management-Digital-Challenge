package com.db.awmd.challenge.web;

import com.db.awmd.challenge.model.TransferMoneyRequest;
import com.db.awmd.challenge.model.TransferMoneyResponse;
import com.db.awmd.challenge.service.TransferMoneyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/*
* Rest Controller API that is responsible  Money Transfer from one Acount to Another Acount
*/
@RestController
@RequestMapping("/v1")
public class TransferMoneyController {

    private final TransferMoneyService transferService;

    public TransferMoneyController(TransferMoneyService transferService) {
        this.transferService = transferService;
    }

    @PostMapping(value = "/transfer-money",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransferMoneyResponse> transferMoney(@RequestBody @Valid TransferMoneyRequest transferRequest) {
        if(transferRequest.getTransferredAccountId().equals(transferRequest.getTransferrerAccountId())){
            TransferMoneyResponse response=new TransferMoneyResponse();
            response.setResponseCode(400);
            response.setMessage("Acount From and To Should not be same");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        TransferMoneyResponse transferResponse = transferService.transferMoney(transferRequest);
        return ResponseEntity.ok(transferResponse);
    }
}
