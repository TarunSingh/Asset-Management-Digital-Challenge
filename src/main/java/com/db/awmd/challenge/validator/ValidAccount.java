package com.db.awmd.challenge.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AccountValidatorImpl.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidAccount {
    String message() default "{account.notfound}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
