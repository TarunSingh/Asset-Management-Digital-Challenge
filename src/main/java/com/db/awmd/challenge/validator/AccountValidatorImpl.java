package com.db.awmd.challenge.validator;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.repository.AccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
/*
*   Custom Validator to validate an account number.
*/
public class AccountValidatorImpl implements ConstraintValidator<ValidAccount, String> {
    @Autowired
    private AccountsRepository accountRepository;
    @Override
    public void initialize(ValidAccount paramA) {
    }

    @Override
    public boolean isValid(String accountId, ConstraintValidatorContext ctx) {
        if(accountId == null){
            return false;
        }
        Account account = accountRepository.getAccount(accountId);
        //Account Not Found
        if (account == null) {
            return false;
        }
        return true;
    }

}