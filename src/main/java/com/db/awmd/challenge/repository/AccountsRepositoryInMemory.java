package com.db.awmd.challenge.repository;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class AccountsRepositoryInMemory implements AccountsRepository {


    private final Map<String, Account> accounts = new ConcurrentHashMap<>();


    @Override
    public void createAccount(Account account) throws DuplicateAccountIdException {
        Account previousAccount = accounts.putIfAbsent(account.getAccountId(), account);
        if (previousAccount != null) {
            throw new DuplicateAccountIdException("Account id " + account.getAccountId() + " already exists!");
        }
    }

    @Override
    public Account getAccount(String accountId) {
        return accounts.get(accountId);
    }

    @Override
    public void clearAccounts() {
        accounts.clear();
    }

    /*
         Update both acounts When Money is getting transfer from one Account to Another Acount
         This Method Should Be syncronised so that so other transaction can happen using that account.
    */
    @Override
    public synchronized void updateAcount(Account transferrerAccount, Account transferredAccount, BigDecimal transferAmount) {
        transferrerAccount.debitBalance(transferAmount);
        transferredAccount.creditBalance(transferAmount);
        accounts.put(transferrerAccount.getAccountId(), transferrerAccount);
        accounts.put(transferredAccount.getAccountId(), transferredAccount);

    }
}
