package com.db.awmd.challenge;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.model.TransferMoneyRequest;
import com.db.awmd.challenge.model.TransferMoneyResponse;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.service.TransferMoneyService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class TransferMoneyControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private AccountsService accountsService;

    @Autowired
    private TransferMoneyService transferMoneyService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void prepareMockMvc() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
        transferMoneyService= Mockito.mock(TransferMoneyService.class);
    }

    @Test
    public void transferMoneySuccessTest() throws Exception {
        createAccount();
        Mockito.when(transferMoneyService.transferMoney(Mockito.any(TransferMoneyRequest.class))).thenReturn(transferMoneyResponse());
        this.mockMvc.perform(post("/v1/transfer-money").contentType(MediaType.APPLICATION_JSON)
                .content("{\"accountFrom\":4,\"accountTo\":1,\"transfer-amount\":100}")).andExpect(status().isOk());
        accountsService.getAccountsRepository().clearAccounts();
    }

    @Test
    public void accountNotFoundWhileMoneyTransferTest() throws Exception {
        createAccount();
        Mockito.when(transferMoneyService.transferMoney(Mockito.any(TransferMoneyRequest.class))).thenReturn(transferMoneyResponse());
        this.mockMvc.perform(post("/v1/transfer-money").contentType(MediaType.APPLICATION_JSON)
                .content("{\"accountFrom\":5,\"accountTo\":1,\"transfer-amount\":100}")).andExpect(status().isBadRequest());
        accountsService.getAccountsRepository().clearAccounts();
    }

    @Test
    public void negativeMoneyTransferTest() throws Exception {
        accountsService.getAccountsRepository().clearAccounts();
        createAccount();
        Mockito.when(transferMoneyService.transferMoney(Mockito.any(TransferMoneyRequest.class))).thenReturn(transferMoneyResponse());
        this.mockMvc.perform(post("/v1/transfer-money").contentType(MediaType.APPLICATION_JSON)
                .content("{\"accountFrom\":4,\"accountTo\":1,\"transfer-amount\":-120}")).andExpect(status().isBadRequest());
        accountsService.getAccountsRepository().clearAccounts();
    }
    @Test
    public void sameAccountTransferTest() throws Exception {
        accountsService.getAccountsRepository().clearAccounts();
        createAccount();
        Mockito.when(transferMoneyService.transferMoney(Mockito.any(TransferMoneyRequest.class))).thenReturn(transferMoneyResponse());
        this.mockMvc.perform(post("/v1/transfer-money").contentType(MediaType.APPLICATION_JSON)
                .content("{\"accountFrom\":1,\"accountTo\":1,\"transfer-amount\":1}")).andExpect(status().isBadRequest());
        accountsService.getAccountsRepository().clearAccounts();
    }
    private TransferMoneyResponse transferMoneyResponse(){
        TransferMoneyResponse response=new TransferMoneyResponse();
        response.setMessage("Success");
        response.setResponseCode(200);
        return response;
    }
    private void  createAccount(){
        Account account = new Account("4");
        account.setBalance(new BigDecimal(1000));
        this.accountsService.createAccount(account);
        Account account1 = new Account("1");
        account.setBalance(new BigDecimal(1000));
        this.accountsService.createAccount(account1);
    }
}
